from unittest import TestCase

from recipes.models import Ingredient, Recipe
from recipes.templatetags.resizer import resize_to


class ResizeToTests(TestCase):
    def test_error_when_ingredient_is_none(self):
        with self.assertRaises(AttributeError):
            resize_to(None, 3)

    def test_recipe_has_no_serving(self):
        recipe = Recipe(servings=None)
        ingredient = Ingredient(recipe=recipe, amount=5)
        result = resize_to(ingredient, None)
        self.assertEqual(5, result)

    def test_resize_to_is_none(self):
        recipe = Recipe(servings=6)
        ingredient = Ingredient(recipe=recipe, amount=5)
        result = resize_to(ingredient, None)
        self.assertEqual(result, 5)

    def test_with_servings_and_resize_to(self):
        recipe = Recipe(servings=6)
        ingredient = Ingredient(recipe=recipe, amount=10)
        result = resize_to(ingredient, 12)
        self.assertEqual(result, 20)
