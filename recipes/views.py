from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import IntegrityError
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView

from recipes.forms import RatingForm
from recipes.models import Ingredient, Recipe, ShoppingItem


def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
        if form.is_valid():
            try:
                recipe = Recipe.objects.get(pk=recipe_id)
                rating = form.save(commit=False)
                rating.recipe = recipe
                rating.save()
            except Recipe.DoesNotExist:
                return redirect("recipes_list")
    return redirect("recipe_detail", pk=recipe_id)


def create_shopping_item(request):
    ingredient_id = request.POST.get("ingredient_id")
    ingredient = Ingredient.objects.get(id=ingredient_id)
    user = request.user
    try:
        ShoppingItem.objects.create(food=ingredient.food, user=user)

    except IntegrityError:
        pass
    return redirect("recipe_detail", pk=ingredient.recipe.id)


def delete_shopping_items(request):
    user = request.user
    try:
        ShoppingItem.objects.filter(user=user).delete()

    except IntegrityError:
        pass
    return redirect("shopping_list")


class ShoppingItemListView(ListView):
    model = ShoppingItem
    template_name = "shopping_list/list.html"
    paginate_by = 2
    context_object_name = "shopping_list"

    def get_queryset(self):
        return ShoppingItem.objects.filter(user=self.request.user)


class RecipeListView(ListView):
    model = Recipe
    template_name = "recipes/list.html"
    paginate_by = 2


class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipes/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        shopping_items = user.shopping_items.all() if user.id else []
        foods = [item.food for item in shopping_items]
        context["foods"] = foods
        context["rating_form"] = RatingForm()
        context["servings"] = self.request.GET.get("servings")
        return context


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = ["name", "description", "image", "servings"]
    success_url = reverse_lazy("recipes_list")
    login_url = "login"
    redirect_field_name = "redirect_to"

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    fields = ["name", "description", "image", "servings"]
    success_url = reverse_lazy("recipes_list")
    login_url = "login"
    redirect_field_name = "redirect_to"


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")
    login_url = "login"
    redirect_field_name = "redirect_to"
