# Generated by Django 4.0.3 on 2022-09-29 18:47

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0002_shoppingitem'),
    ]

    operations = [
        migrations.RenameField(
            model_name='shoppingitem',
            old_name='foodItem',
            new_name='food',
        ),
    ]
