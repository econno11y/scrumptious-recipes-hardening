from django import template

register = template.Library()


def resize_to(ingredient, target):
    servings = ingredient.recipe.servings
    if servings and target:
        return ingredient.amount * int(target) / servings
    return ingredient.amount


register.filter(resize_to)
