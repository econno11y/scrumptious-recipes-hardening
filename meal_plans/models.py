from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

USER_MODEL = settings.AUTH_USER_MODEL


# Create your models here.
class MealPlan(models.Model):
    name = models.TextField()
    date = models.DateField()
    owner = models.ForeignKey(
        USER_MODEL,
        related_name="mealplans",
        on_delete=models.CASCADE,
        null=True,
    )

    recipes = models.ManyToManyField(
        "recipes.Recipe", related_name="meal_plans"
    )

    def __str__(self):
        return self.name + " by " + str(self.owner)
