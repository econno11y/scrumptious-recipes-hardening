from django import forms
from recipes.models import Recipe

from .models import MealPlan


class CreateMealPlanForm(forms.ModelForm):
    class Meta:
        model = MealPlan
        fields = ["name", "date", "recipes"]
